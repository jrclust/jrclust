[JRCLUST change log]
James Jun, HHMI-Janelia Research Campus, Applied Physics and Instrumentation Group

--------------------------------------------------------------------
[2017/6/22]
"traces" command now supports multiple time segments (nTime_traces parameter).
  nTime_traces = 1 shows a single time segment as previously.
GPU is disabled for manual curation (jrc manual)

[2017/6/19]
Code dependencies to external functions removed in jrc2 and jrc3.
Run "jrc3 dependencies" to show dependencies to toolboxes and other functions.

[2017/6/15]
Paged load uses overlapping time to reduce missed spikes at the edges due to filter edge effect.
  "nPad_filt" controls number of samples for overlap.
  This feature is added to both jrc2 and jrc3.

[2017/6/14]
JRCLUST version 3 is released. Run "jrc3" or "jrc" to use this version.
  Substantial improvement with dataset with probe drift. 
    Set "nTime_clu" to 1 if no drift occurs, 2 or higher if drift occurs fast
  "jrc3 detect" runs spike detection and feature extraction.
  "jrc3 sort" runs clustering and posthoc merging.
  "jrc3 auto" runs posthoc merging.
  "jrc3 manual, spikesort-manual, sort-manual, or auto-manual" runs manual UI.
Better global noise rejection by blanking noisy period.
  "blank_thresh" sets the MAD threshold for the channel mean-based activity rejection
  "blank_period_ms" sets the duration of the blanking. Useful to remove licking or motion artifacts  


[2017/6/6]
"jrc2 auto" command is added. This performs re-clustering after updating the
  post-clustering parameters such as "delta1_cut" (cluster confidence).
  This must be done after running "jrc2 sort" or "jrc2 spikesort".
  This command uses the features previously computed.
Local CAR now uses "nSites_ref" channels with the least activities (lowest stdev).
  Previously reference sites were selected by choosing four furthest sites from the center.
If vcSpkRef="nmean" is used, the features from the local reference sites are set to zero.
  This excludes features from the sites with low SNR.

[2017/6/4]
Robust GPU memory processing. If GPU memory error occurs during spike detection the 
  processing is performed in CPU after transfered the data to the main memory 

[2017/5/29]
Default setting changed to produce lower false negative under drift.
  Now checks 2x greater spatial range. This doesn't hurt performance for non-drifting dataset
Post-cluster waveform-based merging became 2x faster.
  Now checks 2x greater spatial range for merging. 
  A cluster pair must have at least 4 overlapping sites

[2017/5/24]
Document updated. Output files and variables described. 
  Type "jrc2 doc" to view the PDF manual.
Improved history logging behavior
  All manual operations are stored in "cS_log" variable.
  Manual operations are reset after automated sorting.
  Ten last performed operations are stored in RAM. 
  The number of logged entries can be changed in "MAX_LOG" parameter.
  Lastest program state is automatically saved to the disk for each manual operation.

[2017/5/12]
Bugfix: Spike detection error fixed when refrac_factor >1 was used
  The previous version assigned an incorrect site # for the spike being detected.
  This is a serious bug and update is recomended.
Spike merging speed is improved.

[2017/5/6]
Bugfix: Multishank post-hoc merging error fixed
Added jrc2 batch-mat myfile_batch.mat command
 myfile_batch.mat contains csFile_bin (binary files) and csFile_template (template files)
 csFile_template can be a string or cell strings
 Optionally provide csFile_prb (probe files), which can be a string or cell strings (for each .bin file)

[2017/5/5]
History menu is added to GUI. This feature allows you to undo/redo manual actions,
  and restore to previous state if JRCLUST crashes. Restart JRCLUST and select 
  the last action saved in the "History" menu (left of Help menu).

[2017/5/3]
Bugfix: New update (4/26) can display older format saved.
Faster post-hoc merging
Faster merging and splitting
Improved visualization for the cluster correlation matrix (FigWavCor)

[2017/4/26]
'nneigh_min_detect' parameter is introduced. This sets the minimum number of neighbors around spikes
  below the negative detection threshold. Set between [0,1,2]. 1 is recommended.
  Previous version used hard-coded value of 2 which lead to missing spikes.

[2017/4/25]
'nDiff_filt' parameter is intorduced to control the smoothing parameter of the differentiator filter.   
  Set nDiff_filt between 1-4. 3 is recommended. Higher number results in more agressive smoothing. 
  Previous version used 4 but it smoothed too aggressively leading to loss of small and narrow spike waveforms
  To skip differentiator sgfilter, set nDiff_filt = []; 
'tlim_load' parameter is now enabled, which sets the time range of data to load (in sec unit).
  For multiple file merging, the time base refers to each individual file and it applies for all files being loaded

[2017/4/21]
Bugfix: Split selection refinement fixed (polygon adjustment didn't take effect)
Bugfix: Raw waveform display works with the dataset sorted by the previous version 
Spike position view (bottom-left) shows 4x more spikes
Color can be changed for projection view by changing 'mrColor_proj' in the .prm file
  mrColor_proj = [r0 g0 b0; r1 g1 b1; r2 g2 b2]; % 0: background; 1: Cluster selected; 2: Cluster compared

[2017/4/20]
Multiple feature viewing enabled. Go to menu and select 'Projection' to change features to display
  available features: {'vpp', 'cov', 'pca'}. This affects the projection and time figures.
  To change a default feature to display, set 'vcFet_show' to one of {'vpp', 'cov', 'pca'} in the .prm file.

[2017/4/19]
Raw waveform is enabled for manual clustering. set 'fWav_raw_show=1' to enable raw waveform view.

[2017/4/5]
Instruction manual updated (install.txt). Updated installation from the website (jrc2.zip).
Added feature export command. 
  Run "jrc2 export-fet" and this outputs mrFet, miFet_sites to the workspace.

[2017/4/4]
Trace view: For multi-file sorting, you can select files from the command line 
    instead of from the dialog box, which sometimes run out of the monitor space.
    Alternatively, you can directly select a file to display by running
    "jrc2 traces myparam.prm File#"
Bugfix: Projection view polygon cutting resulted in empty view when diagonal 
    channel views are selected. Now fixed.

[2017/4/2]
Bug-fix: cluster restore error resolved in "jrc2 manual".
Speed-up: post-cluster step is 2x faster thanks to parallelization.

[2017/03/31]
Added feature: exporting spike amplitudes in uV unit, organized by clusters
    "jrc2 export-spkamp myparam.prm (clu#)"
    This outputs Vpp, Vmin, Vmax to the workspace (cmrVpp_clu, cmrVmin_clu, cmrVmax_clu)

[2017/03/29]
Compiled for GPU with >4GB memory (nvcc -m 64 -arch sm_35)
    Supporting NVIDIA GPU with compute capatiliby 3.5 or later
    Kepler, Maxwell and Pascal architecture (circa 2013, GeForce 700 series or later)
UI time view debugged. Channel switching behavior fixed.

[2017/03/28]
UI error fixed: Waveform error after splitting/merging
Kilosort compilation script
"jrc2 export-spkwav" command added. This exports spike waveforms from all clusters.
    run "jrc2 export-spkwav unit#" to export and plot spikes from a specified unit#.

[2017/03/26]
Significant improvement in UI startup and cluster selection speed.
Waveform display fixed. Previously displayed differentiated waveform.

[2017/03/24]
Fixed a slow startup for jrc2 manual

[2017/03/23]
Faster plot in "jrc2 manual". Number of plot objects in FigWav reduced to 10.
Export to version 1 format supported via "jrc2 export-jrc1 myparam.prm" command.
Menu commands are included in the unit test.
CUDA compile script supported (jrc2 compile).

[2017/03/22]
Waveform fix after splitting in the FigPos window (left-bottom).
"jrc2 probe" remembers the current prm file
Split in the projection view, polygon adjustment now editable
Kilosort integrated. run "jrc2 kilosort myparam.prm", and run "jrc2 manual" for manual merging/splitting


[2017/03/17]
Export-csv feature fixed
Clusters are indicated as randomized but persistent color and line width for each spike

[2017/03/14]
Code clean-up. jrc2.m dependency was reduced to ~5 external functions.

[2017/03/12]
UI bug fix (jrc2 manual)
    UI automated testing implemented
    Merge and split errors fixed
UI feature improvements
    Amplitude zoom change decoupled beteween waveform, time and projection views
Multiple file processing supported (set csFiles_merge = {'file1', 'file2, ...} or csFiles_merge = myfiles*.bin).
Multiple shanks supported (edit "shank" field in .prb file, see sample.prb file for example).
    "jrc2 probe" command displays different shanks in unique colors.
    

[2017/02/24]
JRCLUST ver 2 update (run jrc2)
    Unit test added
    Memory-efficient sorting (jrc2)
    Improved trace view (jrc2 traces)

[2017/01/31]
sample.prb fixed
Image processing toolbox requirement specified.

[2017/01/23]
File merging error fixed (LFP files are now merged for Neuropixels probe).
Selective site loading by using a custom-prb file

[2016/12/13]
Added IMEC sync export function. It outputs vnSync variable that contains 16-bit uint16.

[2016/10/30]
Fixed GUI command "File>export waveform". Now correct waveform is saved.

[2016/10/25]
Default parameters changed (delta1_cut, dc_percent) to improve the sorting outcome, 
    now splits clusters less.

[2016/10/20]
jrc makeprm supports merging multiple files for multi-shank probe. 
    You need to create a probe file for each shank to use this feature.
    Copy the probe file(myprobe_shank1.prb) to the jrclust folder and run
    "jrclust makeprm 'c:\directory\data_1*.bin' myprobe_shank1.prb
    This will merge the .bin files to data_1all.bin file and create 
    'data_1all_CNT_4x16probe_shank1.prm' in where the .bin file resides.
    you can then run "jrclust spikesort" to sort the data
You don't need to specify .prm file after you specify once. for example,
    After running "jrclust makeprm myparam.prm", you can simply run
    "jrclust spikesort" and it will automatically use "myparam.prm".
You can use "jrc" instead of "jrclust".
Template file is now supported. Try
    "jrclust makeprm rawdata.bin myprobe.prb my_template.prm"

[2016/10/11]
jrc manual debugged. The waveforms are now scaled and displayed correctly. Previously the software showed differentiated waveform when the differentiation filter was enabled.

[2016/10/9]
Added FFT-cleanup procedure which removes narrow-band noise. set 'fft_thresh=0' to disable. fft_thresh=10 recommended (z-score threshold).
More efficient handle on the raw waveform (S0.mrWav moved to global variable).

[2016/10/5]
More efficient local common average referencing
Improved feature used by default (spacetime), which uses physical position of spikes to cluster.

[2016/9/29]
Default settings updated
Spacetime feature uses minimum amplitude

[2016/9/27]
Waveform error fixed after split and merge in UI.
Faster waveform calculation.

[2016/9/22]
Fixed waveform shape. Now it shows non-differentiated waveform when 'spacetime' feature is used.
Faster load for raw traces.
Improved time alignment

[2016/9/20]
GPU errror during filtering fixed.
Default settings changed.

[2016/8/30]
Projection split error fixed.
Added "auto-correlation" feature. (vcFet = 'acor')

[2016/8/28]
Background noise cancellation (set fMeanSite=2 for local median subtraction)
Added 'moment' feature. This projects spikes to mean, sqrt(mean), mean.^2

[2016/8/21]
2x faster file loading using GPU for combining loading/processing operation.
2x faster and more accurate spike detection.
Currently process 5x realtime speed for 120 channels, 1.2x realtime for 374 channels

[2016/8/18]
2x faster filtering and spike detection using multiple CPU cores.

[2016/8/17]
"jrclust maketrial" fixed for a version older thatn R2015a.

[2016/8/15]
"jrclust auto" algorithmic update
    Graded rho/delta calculation
    Determine clustered distance distribution
    Use gradual drop-off spatial windowing instead of shart drop-off and increaes the range.
Improved automated clustering (Waveforms are now centered before projecting principal components).
Faster feature computation for manual curation.

[2016/8/14]
Documentation updated. Show the latest documentation by running ("JRCLUST doc").
Fixed "jrclust install" and "jrclust update" commands.
"jrclust makeprm" now supports multi-file merging using a wild-card. 
    Run "jrclust makeprm myfiles_*.bin myprobe.prb" and JRCLUST will create 
    "myfiles_all.bin", "myfiles_all.meta" and "myfiles_all.prm"

[2016/8/12]
Added auto split/merging when 'spca' feature is used ("postCluster.m").
Added an option to use amplitude-scaled dc (distance-cutoff) parameter for R-L clustering.
    Try dc_frac = 1/3; and use (vcCluDist = 'neucldist';). This sets a radius parameter to 
    compute "rho" parameter in R-L clustering for each individual spikes. Small spikes will
    use smaller radius and large spikes will use larger radius scaled by the norm of vector.
    Previously I used the same dc_frac parameter for all spike amplitudes which resulted in
    over-merging for smaller spikes and over-splitting for larger spikes.
Added "jrclust compile" for recompiling the CUDA GPU codes.
Features are not saved but computed on-fly. This saves lots of memory and time.
default.prm updated. Try the new default configuration which allow much 
    1. more sensitive spike detection
    2. much faster and memory-efficient feature handling
    3. more accurate automated sorting.

[2016/8/5]
sPCA feature added (single-channel feature and clustering).
prm loading error fixed.
"jrclust install" script debugged

[2016/8/3]
Debuged spike display of raw, unfiltered waveform.
Debugged traces view, transition from 'a' to 'f'.

[2016/8/2]
Added "Save figures" under File menu ("jrclust manual").
More sensitive spike detection. Set vcSpatialFilter = "mean" and fPcaDetect = 1.
Time view can display second and third principal components ("jrclust manual").
sPCA feature added. Cluster by channel.

[2016/8/1]
Fixed "jrclust makeprm" command
Fixed "jrclust download" command

[2016/7/28]
Reference can be a channel (vcCommonRef = "1" will subtract channel 1 from the rest of channels)

[2016/7/27]
Cut in PCA view (Press 'F' in amplitude projection view or Time view);

[2016/7/15]
Dipole-based unit shift (in time view, press "e")
Unit track and shift feature (in time view, press "c")

[2016/7/22]
Debugged jrclust loadparam behavior (prb file did not load)
Faster Vpp computation and mean waveform bcomputation
Clusters are initially sorted by their centroid locations
A cluster or a pair of clusters can be selected by clicking on the traces or correlation matrix.
"jrclust manual" layout is rearranged

[2016/7/21]
Export waveform in the time window (jrclust manual)
plot traces debugged.
Changed behavior of "jrclust makeprm myrecording.bin myprobe.prb".
    The prb file can contain settings to be copied to myparam.prm.
"jrclust trackset mycollection.set" plots collection of prm files. 
    Refer to "ph3o3.set" for an example.

[2016/7/19]
Debugged trace view zoom and startup ("jrclust traces").
Large time view by skipping spikes every n samples (nSkip_show).
Faster spike display ("jrclust traces").

[2016/7/18]
Feature plot enabled. PCA or features other than Vpp can be plotted.
Speed improvement in correlation matrix computation.
Speed imporvement in projection view.
X-position view added in the time plot (press 'x', 'y', 't' to switch between x,y position and time views).
Common zoom behavior between the waveform and time view (press up/down arrows).
Click on he Wavform correlation view to select clusters.
Waveform view now permits clicking on the traces. Previously this could not select waveforms.
Local common reference debugged (set vcCommonRef='mean' and nSite_ref>0.
Faster disk loading and filtering (~x2 speed-up).
"jrclust cabletest myrecording.bin"  added for Neuropixels phase 3 probe (BIST #7?).

[2016/7/17]
Added track depth menu in "Time vs. Amplitude" view. Press "T" to switch to the "track depth" view.

[2016/7/16]
Debugged jrclust ui, performance improvement for projection window

[2016/7/15]
Cluster exporting menu added in "jrclust manual".
"jrclust download sample" command added. This downloads sample.bin and sample.meta files from Neuropixels Phase 2 probe (128 sites).

[2016/7/13]
vpp feature debug fixed.
Now supports "jrclust makeprm my_recording.bin my_probe.prb"
Debug fix for "jrclust traces" command. Now supports aux. channel view.

[2016/7/12]
Faster trace view interface. Example: "jrclust traces my_param.prm"
UI supports dynamic parameter file updating. No longer need to restart the UI to update prm file.
Edit command added. Example: "jrclust edit myparam.prm".

[2016/7/10]
Much more responsive UI. Significantly faster to update graphics
Cluster Position calculation
Cluster annotation ability

[2016/7/7]
Faster local common average referencing using GPU memory caching (x10 faster).

[2016/06/28]
LFP-based depth tracking. Parameters have "_track" postfix in .prm file.

[2016/06/21]
Local mean subtraction syntax changed (vcCommonRef='mean', nChans_
Goto time added in "traces" view. Press 'G' to go to the start time to plot.
Bad channels being excluded from the computation all together

[2016/06/20]
Memory and speed optimization for

[2016/06/19]
Memory optimization for common mean reference
Local mean subtraction is supported (vcCommonRef = 'localmean')

[2016/06/17]
Can work with files that were stopped abruptly during recording (multiple of nChans requirement relaxed).

[2016/06/10]
+GUI: Press 's' to autosplit in the waveform view
+ added a feature to reject spikes during movement. Movement is detected FROM the global mean across channel (common reference). Motion is defined when the absolute value of the mean across sites exceed 5 SD. 
    Set "thersh_mean_rejectSpk=5" under "detection" section to change the threshold. Set this to 0 to skip motion rejection.
+File saving speed-up. Feature tensor (trFet) is saved in a binary format (.fet).
+PCA speed-up and reduction in memory usage.

[2016/06/09]
Decreased memory use by half. Loading a fraction of a bin file (~1.2 GB) at a time and transposes.

[2016/06/07]
LFP and Aux channel handling updated
Phase 3 probe updated

[2016/05/21]
run "jrclust update" to update from dropbox

[2016/05/18]
Otsu's graythresh method for automatically determining dc_cut. This is recommend instead of distribution based sorting (set vcDc_clu = 'Otsu').
Channel query when viewing traces (jrclust traces). Press 'c' and draw a rectangle to find out a channel number. (useful in LFP view mode).
New clustering feature (vcFet = 'diff248' see http://www.synopsys.com/community/universityprogram/pages/neuralspikesort.aspx). 
    This works very well when combined with Otsu auto-thresholding method.

[2016/05/08]
tlim_clu fixed when fDetrend_postclu is enabled


[2016/04/28]
memory use optimization. load one channel at a time.


--------------------------
[ToDo]
Drift compensation
Choose spike center according to Vpp
projection view using features used for spike-sorting
auto-split keyboard shortcut and make it faster
memory optimization by loading bit of waveform at a time
annotate clusters
warning message for overwrite
LFP phase analysis integration for jrclust_ui
 place cell integration
cut in rho-delta view
click on a matrix view to show tw pairs
quality score display
store metafile in P structure. save as raw text
realtime spike sorting
manual command startup speedup

[ToDo, Tahl Holtzman]
Waveform width measurement
Toggle between positive and negative clusters (give access to deleted clusters)
relate spike color in the trace view and cluster number

[Todo, Joao, 2016 06 17]
resample in xy projection 'a' 
self similarity in time by dividing into different epocs. 
spike sorting mean waveform output
display the split in the features space

[Anupan, 2016 07 15]
jrclust manual: debug when split
export text field to csv file

---------------------------------------------

@TODO: Before clustering determine viSite_spk based on x,y centroid (nearest site)
@TODO: Depth shift
@TODO: cut in the position view
@TODO: Trace view spike in lfp view and hiding spikes
@TODO: Fix sitestat
@TODO: Auto-split and merging after running clusering
@TODO: R-L cluster: add max. time neighborhood, fix the neighbor size. 
@TODO: extreme-drift correction. Track global drift and shift.
